import React, {Component} from 'react';
import Square from '../square'

class Board extends Component {
    renderSquare(x, y) {
      return <Square valuex={x} valuey={y}/>;
    }

    defineArray(numOfTimes) {
        let arrayDone = [];
        for (let i = 0; i < numOfTimes; i++) {
            arrayDone.push(i);
          }
        return  arrayDone;
    }

  
    render() {
      const status = 'Next player: X';
      let numOfRow = 8;
      let numOfCol = 10;
      let arrayRow =  this.defineArray(numOfRow);
      let arrayCol = this.defineArray(numOfCol);

      return (
        <div className="game-board">
          {arrayRow.map((x, i) =>
            <div className={"board-row " + i}>
              {arrayCol.map((y, i) =>
                this.renderSquare(x, y)
              )}
            </div>
          )}
        </div >
      );
    }
  }

  export default Board;