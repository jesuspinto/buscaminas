import React, {Component} from 'react';
import ReactDom from 'react-dom';
import Game from './game';

  // ========================================
  
document.addEventListener('DOMContentLoaded',() => {
  ReactDom.render(
    <Game />,
    document.getElementById('app')
  );
});